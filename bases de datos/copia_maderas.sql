-- MySQL dump 10.13  Distrib 5.5.8, for Win32 (x86)
--
-- Host: localhost    Database: maderas
-- ------------------------------------------------------
-- Server version	5.5.8-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `IdCliente` bigint(20) NOT NULL,
  `NombreCli` varchar(100) NOT NULL,
  `ApellidoCli` varchar(100) DEFAULT NULL,
  `CorreoCli` varchar(50) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `TelefonoCli` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `IdContrato` bigint(20) NOT NULL,
  `FechaCon` date DEFAULT NULL,
  `FkIdCliente` bigint(20) NOT NULL,
  PRIMARY KEY (`IdContrato`),
  KEY `clientecontrato` (`FkIdCliente`),
  CONSTRAINT `contrato_ibfk_1` FOREIGN KEY (`FkIdCliente`) REFERENCES `cliente` (`IdCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallecontrato`
--

DROP TABLE IF EXISTS `detallecontrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallecontrato` (
  `IdDetalleContrato` bigint(20) NOT NULL,
  `Estipulacion` varchar(100) NOT NULL,
  `FechaEntrega` date DEFAULT NULL,
  `FkIdContrato` bigint(20) NOT NULL,
  `FkIdProductoFinal` bigint(20) NOT NULL,
  PRIMARY KEY (`IdDetalleContrato`),
  KEY `contratodetalle` (`FkIdContrato`),
  KEY `detallecontrato` (`FkIdProductoFinal`),
  CONSTRAINT `detallecontrato_ibfk_2` FOREIGN KEY (`FkIdProductoFinal`) REFERENCES `productofinal` (`IdProductoFinal`),
  CONSTRAINT `detallecontrato_ibfk_1` FOREIGN KEY (`FkIdContrato`) REFERENCES `contrato` (`IdContrato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallecontrato`
--

LOCK TABLES `detallecontrato` WRITE;
/*!40000 ALTER TABLE `detallecontrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallecontrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallefacturacompra`
--

DROP TABLE IF EXISTS `detallefacturacompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallefacturacompra` (
  `IdDetalleFc` bigint(20) NOT NULL,
  `Cantidad` bigint(20) NOT NULL,
  `ValorTotal` bigint(20) NOT NULL,
  `DescripcionCompra` varchar(150) NOT NULL,
  `FkIdInvProFinal` bigint(20) NOT NULL,
  `FkIdFacturaCompra` bigint(20) NOT NULL,
  PRIMARY KEY (`IdDetalleFc`),
  KEY `productodetalle` (`FkIdInvProFinal`),
  KEY `productofactura` (`FkIdFacturaCompra`),
  CONSTRAINT `detallefacturacompra_ibfk_1` FOREIGN KEY (`FkIdInvProFinal`) REFERENCES `inventarioproductofinal` (`IdInvProFinal`),
  CONSTRAINT `detallefacturacompra_ibfk_2` FOREIGN KEY (`FkIdFacturaCompra`) REFERENCES `facturacompra` (`IdFacturaCompra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallefacturacompra`
--

LOCK TABLES `detallefacturacompra` WRITE;
/*!40000 ALTER TABLE `detallefacturacompra` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallefacturacompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `IdEmpleado` bigint(20) NOT NULL,
  `NombreEmple` varchar(50) NOT NULL,
  `ApellidoEmple` varchar(50) DEFAULT NULL,
  `DireccionEmple` varchar(90) NOT NULL,
  `CorreoEmple` varchar(80) DEFAULT NULL,
  `TelefonoEmple` bigint(20) NOT NULL,
  PRIMARY KEY (`IdEmpleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturacompra`
--

DROP TABLE IF EXISTS `facturacompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturacompra` (
  `IdFacturaCompra` bigint(20) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `FkIdProveedor` bigint(20) NOT NULL,
  PRIMARY KEY (`IdFacturaCompra`),
  KEY `facturaproveedor` (`FkIdProveedor`),
  CONSTRAINT `facturacompra_ibfk_1` FOREIGN KEY (`FkIdProveedor`) REFERENCES `proveedor` (`IdProveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturacompra`
--

LOCK TABLES `facturacompra` WRITE;
/*!40000 ALTER TABLE `facturacompra` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturacompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturaventa`
--

DROP TABLE IF EXISTS `facturaventa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturaventa` (
  `IdFacturaVenta` bigint(20) NOT NULL,
  `FormaPago` varchar(50) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `FkIdCliente` bigint(20) NOT NULL,
  `FkIdEmpleado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdFacturaVenta`),
  KEY `facturaempleado` (`FkIdEmpleado`),
  KEY `facturacliente` (`FkIdCliente`),
  CONSTRAINT `facturaventa_ibfk_2` FOREIGN KEY (`FkIdCliente`) REFERENCES `cliente` (`IdCliente`),
  CONSTRAINT `facturaventa_ibfk_1` FOREIGN KEY (`FkIdEmpleado`) REFERENCES `empleado` (`IdEmpleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturaventa`
--

LOCK TABLES `facturaventa` WRITE;
/*!40000 ALTER TABLE `facturaventa` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturaventa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventariomateriaprima`
--

DROP TABLE IF EXISTS `inventariomateriaprima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventariomateriaprima` (
  `IdInvMateria` bigint(20) NOT NULL,
  `Cantidad` bigint(20) DEFAULT NULL,
  `StockMinimo` bigint(20) DEFAULT NULL,
  `Descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdInvMateria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventariomateriaprima`
--

LOCK TABLES `inventariomateriaprima` WRITE;
/*!40000 ALTER TABLE `inventariomateriaprima` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventariomateriaprima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventarioproductofinal`
--

DROP TABLE IF EXISTS `inventarioproductofinal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventarioproductofinal` (
  `IdInvProFinal` bigint(20) NOT NULL,
  `Cantidad` bigint(20) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `StockMinimo` bigint(20) NOT NULL,
  `StockMaximo` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdInvProFinal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventarioproductofinal`
--

LOCK TABLES `inventarioproductofinal` WRITE;
/*!40000 ALTER TABLE `inventarioproductofinal` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventarioproductofinal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materiaprima`
--

DROP TABLE IF EXISTS `materiaprima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materiaprima` (
  `IdMateriaPrima` bigint(20) NOT NULL,
  `NombreMateria` varchar(100) NOT NULL,
  `Cantidad` bigint(20) DEFAULT NULL,
  `Descripcion` varchar(150) NOT NULL,
  `FkIdInvMateria` bigint(20) NOT NULL,
  `FkIdProveedor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdMateriaPrima`),
  KEY `inventariomateria` (`FkIdInvMateria`),
  KEY `MAteriaProveedor` (`FkIdProveedor`),
  CONSTRAINT `materiaprima_ibfk_2` FOREIGN KEY (`FkIdProveedor`) REFERENCES `proveedor` (`IdProveedor`),
  CONSTRAINT `materiaprima_ibfk_1` FOREIGN KEY (`FkIdInvMateria`) REFERENCES `inventariomateriaprima` (`IdInvMateria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materiaprima`
--

LOCK TABLES `materiaprima` WRITE;
/*!40000 ALTER TABLE `materiaprima` DISABLE KEYS */;
/*!40000 ALTER TABLE `materiaprima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordenproduccion`
--

DROP TABLE IF EXISTS `ordenproduccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordenproduccion` (
  `IdOrdenProduccion` bigint(20) NOT NULL,
  `Proceso` varchar(200) NOT NULL,
  `Materiales` varchar(200) NOT NULL,
  `FkIdMateriaPrima` bigint(20) NOT NULL,
  PRIMARY KEY (`IdOrdenProduccion`),
  KEY `ordenmateria` (`FkIdMateriaPrima`),
  CONSTRAINT `ordenproduccion_ibfk_1` FOREIGN KEY (`FkIdMateriaPrima`) REFERENCES `materiaprima` (`IdMateriaPrima`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordenproduccion`
--

LOCK TABLES `ordenproduccion` WRITE;
/*!40000 ALTER TABLE `ordenproduccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordenproduccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productofinal`
--

DROP TABLE IF EXISTS `productofinal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productofinal` (
  `IdProductoFinal` bigint(20) NOT NULL,
  `DescripcionPF` varchar(100) NOT NULL,
  `PrecioUnitarioPF` bigint(20) NOT NULL,
  `CodigoOrden` bigint(20) NOT NULL,
  `FkInvfinal` bigint(20) NOT NULL,
  PRIMARY KEY (`IdProductoFinal`),
  KEY `inventariofinal` (`FkInvfinal`),
  CONSTRAINT `productofinal_ibfk_1` FOREIGN KEY (`FkInvfinal`) REFERENCES `inventarioproductofinal` (`IdInvProFinal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productofinal`
--

LOCK TABLES `productofinal` WRITE;
/*!40000 ALTER TABLE `productofinal` DISABLE KEYS */;
/*!40000 ALTER TABLE `productofinal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `IdProveedor` bigint(20) NOT NULL,
  `NombreProv` varchar(50) NOT NULL,
  `ApellidoProv` varchar(50) DEFAULT NULL,
  `TelefonoProv` bigint(20) NOT NULL,
  `CorreoProv` varchar(50) NOT NULL,
  `DireccionProv` varchar(100) NOT NULL,
  PRIMARY KEY (`IdProveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-08 20:32:28
