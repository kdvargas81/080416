-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: proyecto
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudad` (
  `IdCiudad` bigint(20) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `FkIdProyecto` bigint(20) NOT NULL,
  PRIMARY KEY (`IdCiudad`),
  KEY `ciudadproyecto` (`FkIdProyecto`),
  CONSTRAINT `ciudad_ibfk_1` FOREIGN KEY (`FkIdProyecto`) REFERENCES `proyecto` (`IdProyecto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `IdCliente` bigint(20) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Apellido` varchar(50) DEFAULT NULL,
  `Telefono` bigint(20) NOT NULL,
  `EMail` varchar(100) DEFAULT NULL,
  `Direccion` varchar(100) DEFAULT NULL,
  `Cargo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`IdCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `IdContrato` bigint(20) NOT NULL,
  `Fecha` date DEFAULT NULL,
  PRIMARY KEY (`IdContrato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallecontrato`
--

DROP TABLE IF EXISTS `detallecontrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallecontrato` (
  `IdDetalleC` bigint(20) NOT NULL,
  `Estipulacion` varchar(200) NOT NULL,
  `FechaEntrega` date DEFAULT NULL,
  `FkIdContrato` bigint(20) NOT NULL,
  `FkIdProyecto` bigint(20) NOT NULL,
  PRIMARY KEY (`IdDetalleC`),
  KEY `detallecontrato` (`FkIdContrato`),
  KEY `detallepro` (`FkIdProyecto`),
  CONSTRAINT `detallecontrato_ibfk_2` FOREIGN KEY (`FkIdProyecto`) REFERENCES `proyecto` (`IdProyecto`),
  CONSTRAINT `detallecontrato_ibfk_1` FOREIGN KEY (`FkIdContrato`) REFERENCES `contrato` (`IdContrato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallecontrato`
--

LOCK TABLES `detallecontrato` WRITE;
/*!40000 ALTER TABLE `detallecontrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallecontrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallefc`
--

DROP TABLE IF EXISTS `detallefc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallefc` (
  `IdDetalleFC` bigint(20) NOT NULL,
  `Cantidad` bigint(20) NOT NULL,
  `ValorTotal` bigint(20) NOT NULL,
  `Calidad` varchar(100) NOT NULL,
  `Iva` bigint(20) NOT NULL,
  `FkIdFacturaCompra` bigint(20) DEFAULT NULL,
  `FkIdMaterial` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdDetalleFC`),
  KEY `detallefactura` (`FkIdFacturaCompra`),
  KEY `materialdetalle` (`FkIdMaterial`),
  CONSTRAINT `detallefc_ibfk_2` FOREIGN KEY (`FkIdMaterial`) REFERENCES `materiales` (`IdMaterial`),
  CONSTRAINT `detallefc_ibfk_1` FOREIGN KEY (`FkIdFacturaCompra`) REFERENCES `facturacompra` (`IdFacturaCompra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallefc`
--

LOCK TABLES `detallefc` WRITE;
/*!40000 ALTER TABLE `detallefc` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallefc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallefv`
--

DROP TABLE IF EXISTS `detallefv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallefv` (
  `IdDetalleFV` bigint(20) NOT NULL,
  `Cantidad` bigint(20) NOT NULL,
  `ValorTotal` bigint(20) NOT NULL,
  `FkIdProyecto` bigint(20) NOT NULL,
  `FkIdFacturaVenta` bigint(20) NOT NULL,
  PRIMARY KEY (`IdDetalleFV`),
  KEY `detalleproyecto` (`FkIdProyecto`),
  KEY `detallefactura` (`FkIdFacturaVenta`),
  CONSTRAINT `detallefv_ibfk_1` FOREIGN KEY (`FkIdProyecto`) REFERENCES `proyecto` (`IdProyecto`),
  CONSTRAINT `detallefv_ibfk_2` FOREIGN KEY (`FkIdFacturaVenta`) REFERENCES `facturaventa` (`IdFacturaVenta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallefv`
--

LOCK TABLES `detallefv` WRITE;
/*!40000 ALTER TABLE `detallefv` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallefv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalleproyecto`
--

DROP TABLE IF EXISTS `detalleproyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalleproyecto` (
  `IdEmpleadoProyecto` bigint(20) NOT NULL,
  `Rol` varchar(50) NOT NULL,
  `HorasAsignadas` bigint(20) NOT NULL,
  `HorasEjecutadas` bigint(20) NOT NULL,
  `HorasRestantes` bigint(20) NOT NULL,
  `FkIdEmpleado` bigint(20) NOT NULL,
  `FkIdProyecto` bigint(20) NOT NULL,
  PRIMARY KEY (`IdEmpleadoProyecto`),
  KEY `detalleempleado` (`FkIdEmpleado`),
  KEY `detalleproyecto` (`FkIdProyecto`),
  CONSTRAINT `detalleproyecto_ibfk_1` FOREIGN KEY (`FkIdEmpleado`) REFERENCES `empleado` (`IdEmpleado`),
  CONSTRAINT `detalleproyecto_ibfk_2` FOREIGN KEY (`FkIdProyecto`) REFERENCES `proyecto` (`IdProyecto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalleproyecto`
--

LOCK TABLES `detalleproyecto` WRITE;
/*!40000 ALTER TABLE `detalleproyecto` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalleproyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `IdEmpleado` bigint(20) NOT NULL,
  `NombreEmple` varchar(50) NOT NULL,
  `ApellidoEmple` varchar(100) DEFAULT NULL,
  `TelefonoEmple` bigint(20) NOT NULL,
  `DireccionEmple` varchar(100) DEFAULT NULL,
  `EmailEmple` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdEmpleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturacompra`
--

DROP TABLE IF EXISTS `facturacompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturacompra` (
  `IdFacturaCompra` bigint(20) NOT NULL,
  `FormaDePago` varchar(100) NOT NULL,
  `FechaCompra` date DEFAULT NULL,
  `DescripcionCompra` varchar(200) NOT NULL,
  `ValorTotal` bigint(20) NOT NULL,
  `FkNit` bigint(20) NOT NULL,
  PRIMARY KEY (`IdFacturaCompra`),
  KEY `proveedorfactura` (`FkNit`),
  CONSTRAINT `facturacompra_ibfk_1` FOREIGN KEY (`FkNit`) REFERENCES `proveedor` (`Nit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturacompra`
--

LOCK TABLES `facturacompra` WRITE;
/*!40000 ALTER TABLE `facturacompra` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturacompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturaventa`
--

DROP TABLE IF EXISTS `facturaventa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturaventa` (
  `IdFacturaVenta` bigint(20) NOT NULL,
  `FormaPago` bigint(20) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `FkIdCliente` bigint(20) NOT NULL,
  `FkIdEmpleado` bigint(20) NOT NULL,
  PRIMARY KEY (`IdFacturaVenta`),
  KEY `facturacliente` (`FkIdCliente`),
  KEY `facturaempleado` (`FkIdEmpleado`),
  CONSTRAINT `facturaventa_ibfk_1` FOREIGN KEY (`FkIdCliente`) REFERENCES `cliente` (`IdCliente`),
  CONSTRAINT `facturaventa_ibfk_2` FOREIGN KEY (`FkIdEmpleado`) REFERENCES `empleado` (`IdEmpleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturaventa`
--

LOCK TABLES `facturaventa` WRITE;
/*!40000 ALTER TABLE `facturaventa` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturaventa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventarioma`
--

DROP TABLE IF EXISTS `inventarioma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventarioma` (
  `IdInventarioMa` bigint(20) NOT NULL,
  `FechaCreacion` date DEFAULT NULL,
  `Cantidad` bigint(20) NOT NULL,
  `StockMinimo` bigint(20) DEFAULT NULL,
  `StockMaxino` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdInventarioMa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventarioma`
--

LOCK TABLES `inventarioma` WRITE;
/*!40000 ALTER TABLE `inventarioma` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventarioma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materiales`
--

DROP TABLE IF EXISTS `materiales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materiales` (
  `IdMaterial` bigint(20) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Precio` bigint(20) NOT NULL,
  `Descripcion` varchar(100) NOT NULL,
  `FkIdInventarioMa` bigint(20) NOT NULL,
  `FkCodigoOrden` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdMaterial`),
  KEY `inventarioma` (`FkIdInventarioMa`),
  KEY `materialorden` (`FkCodigoOrden`),
  CONSTRAINT `materiales_ibfk_2` FOREIGN KEY (`FkCodigoOrden`) REFERENCES `materiales` (`IdMaterial`),
  CONSTRAINT `materiales_ibfk_1` FOREIGN KEY (`FkIdInventarioMa`) REFERENCES `inventarioma` (`IdInventarioMa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materiales`
--

LOCK TABLES `materiales` WRITE;
/*!40000 ALTER TABLE `materiales` DISABLE KEYS */;
/*!40000 ALTER TABLE `materiales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordenproduccion`
--

DROP TABLE IF EXISTS `ordenproduccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordenproduccion` (
  `CodigoOrden` bigint(20) NOT NULL,
  `Lote` bigint(20) NOT NULL,
  `FechaInicio` date DEFAULT NULL,
  `FechaFinal` date DEFAULT NULL,
  PRIMARY KEY (`CodigoOrden`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordenproduccion`
--

LOCK TABLES `ordenproduccion` WRITE;
/*!40000 ALTER TABLE `ordenproduccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordenproduccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `Nit` bigint(20) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Telefono` bigint(20) NOT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Direccion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Nit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyecto`
--

DROP TABLE IF EXISTS `proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyecto` (
  `IdProyecto` bigint(20) NOT NULL,
  `Codigo` bigint(20) NOT NULL,
  `PrecioUnitario` bigint(20) NOT NULL,
  `Descripcion` varchar(200) NOT NULL,
  `FkCodigoOrden` bigint(20) NOT NULL,
  PRIMARY KEY (`IdProyecto`),
  KEY `proyectoorden` (`FkCodigoOrden`),
  CONSTRAINT `proyecto_ibfk_1` FOREIGN KEY (`FkCodigoOrden`) REFERENCES `ordenproduccion` (`CodigoOrden`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyecto`
--

LOCK TABLES `proyecto` WRITE;
/*!40000 ALTER TABLE `proyecto` DISABLE KEYS */;
/*!40000 ALTER TABLE `proyecto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-11  1:31:54
